package com.douglas.jointlyapp.ui.base;

public interface BasePresenter {
    void onDestroy();
}
