package com.douglas.jointlyapp.ui.base;

import java.util.List;

public interface BaseListView<T> {
    void onSuccess(List<T> list);
}
